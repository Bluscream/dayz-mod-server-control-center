using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;

[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Crosire")]
[assembly: AssemblyFileVersion("5.9.4.1")]
[assembly: AssemblyTitle("DayZ Server Control Center")]
[assembly: AssemblyDescription("DayZ Server Control Center")]
[assembly: AssemblyCopyright("Copyright ©2012-2013 Crosire")]
[assembly: Guid("989e855f-8547-489f-8740-da319dc338f0")]
[assembly: AssemblyProduct("DayZ Server Control Center")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("5.9.4.1")]
