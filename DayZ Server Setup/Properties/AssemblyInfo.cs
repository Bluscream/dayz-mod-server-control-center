using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;

[assembly: AssemblyDescription("Setup for DayZ Control Center")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Crosire")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyTitle("DayZ Server Control Center Setup")]
[assembly: AssemblyProduct("DayZ Server Control Center Setup")]
[assembly: AssemblyCopyright("Copyright ©2012-2013 Crosire")]
[assembly: ComVisible(false)]
[assembly: Guid("a187dc13-2200-4bb4-a44c-2a9fb95c831c")]
[assembly: AssemblyFileVersion("5.9.4.1")]
[assembly: AssemblyVersion("5.9.4.1")]
