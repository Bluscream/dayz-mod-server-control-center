using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DayZ Server Control Center Updater")]
[assembly: AssemblyDescription("Updater for DayZ Control Center")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Crosire")]
[assembly: AssemblyProduct("DayZ Server Control Center Updater")]
[assembly: AssemblyCopyright("Copyright ©2012-2013 Crosire")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("b4ccca99-23ce-48e2-8fad-5198f9d08347")]
[assembly: AssemblyFileVersion("5.9.4.1")]
[assembly: AssemblyVersion("5.9.4.1")]
